let player = {
    name : 'john',
    chips : 500
}


let cards = []
let sum = 0;
let gotBlackJack = false;
let message = '';
let inGame = false;

let playerDetails = document.getElementById("playerInfo");

playerDetails.textContent = player.name + ': $' + player.chips;

function randomCardGenerator(){
    let randomNumber = Math.floor(Math.random()*13) + 1
    if (randomNumber >10){
        return 10
    }
    else if (randomNumber === 1){
        return 11
    }
    else{
        return randomNumber
    }
}

function start(){
    inGame = true
    let firstCard = randomCardGenerator();
    let secondCard = randomCardGenerator();
    cards = [firstCard, secondCard]
    sum = firstCard + secondCard;
    game()

}


function game(){
    
    document.getElementById('cards').textContent = 'Cards : ';
    for (let i = 0;i<cards.length;i++){
        document.getElementById('cards').textContent += cards[i] + ','
    }
    document.getElementById('sum').textContent = 'Sum : ' + sum;

    if(sum<=20){
        message = "Want to draw a new card ?"
    }
    else if(sum==21){
        message = "Congrats! You got the Black Jack !!!!!"
        gotBlackJack = true;
    }
    else{
        message = "You are out of the game !"
        inGame = false;
        
    }
    document.getElementById('question').textContent = message;
}

function newCard(){
    if (inGame === true && gotBlackJack === false){
        let nextCard = randomCardGenerator()
        sum += nextCard
        cards.push(nextCard)
        game()
    }
    

}